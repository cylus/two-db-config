package com.howtodoinjava.app;


import com.howtodoinjava.app.model.policy.Policy;
import com.howtodoinjava.app.model.user.User;
import com.howtodoinjava.app.repositories.policy.PolicyRepository;
import com.howtodoinjava.app.repositories.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class AppTest {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PolicyRepository policyRepository;
  
  @Test
  void saveUserDataInDB() {

    User user = new User();
    user.setEmail("johndoe@gmail.com");
    user.setFirstName("John");
    user.setLastName("DOe");

    User savedUser = userRepository.save(user);
    Optional<User> userData = userRepository.findById(savedUser.getId());
    assertTrue(userData.isPresent());

  }

  @Test
  void saveOrderDataInDB() {

    Policy policy = new Policy();
    policy.setProductName("Mobile");
    policy.setOrderAmount(200);
    policy.setUserId(1);

    Policy savedPolicy = policyRepository.save(policy);
    Optional<Policy> orderData = policyRepository.findById(savedPolicy.getId());
    assertTrue(orderData.isPresent());
  }

}

